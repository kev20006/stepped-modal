import React, {useState, useEffect} from 'react'

import logo from './logo.svg';
import SteppedModal from './SteppedModal/SteppedModal';
import './App.css';

function App() {
  const [stepsComplete, setStepsComplete] = useState([true, false, false, false])
  const [stepOneChecked, setStepOneChecked] = useState(false);
  const [stepTwoString, setStepTwoString] = useState('')
  const [stepThreeString, setStepThreeString] = useState('')
  

  // validate step 1
  useEffect(() => {
    stepOneChecked && setStepsComplete([true, true, false, false])
  }, [stepOneChecked])

  // validate step 2
  useEffect(() => {
    stepTwoString.length >= 10 && setStepsComplete([true, true, true, false])
  }, [stepTwoString])
  
  useEffect(() => {
    stepThreeString.length >= 10 && !stepThreeString.includes('E') && setStepsComplete([true, true, true, true])
  }, [stepThreeString])
  
  return (
    <div className="App">
      <h1>SteppedModal</h1>
      <SteppedModal
        navigation={['step 1', 'step 2', 'step 3', 'step 4']}
        steps={[
          <Step1 key={'step1'} stepOneChecked={stepOneChecked} setStepOneChecked={setStepOneChecked}/>,
          <Step2 key={'step2'} stepTwoString={stepTwoString} setStepTwoString={setStepTwoString} />,
          <Step3 key={'step3'} stepThreeString={stepThreeString} setStepThreeString={setStepThreeString}/>,
          <Step4 key={'step4'} stepTwoString={stepTwoString} stepThreeString={stepThreeString}/>]
        }
        stepsComplete={stepsComplete}
        validation={[stepOneChecked, stepTwoString.length >= 10, stepThreeString.length >= 10 && !stepThreeString.includes('E'), true]}
        submitAction={() => alert('submit form!!')}

      />
    </div>
  );
}

const Step1 = ({stepOneChecked, setStepOneChecked}) => {
  return (
    <>
      <h1>Complete Checkbox to move on!</h1>
      <input
        type='checkbox' 
        value={stepOneChecked}
        onChange={() => setStepOneChecked(!stepOneChecked)}
      />
    </>
    )
}
const Step2 =  ({stepTwoString, setStepTwoString}) => {
  return (
    <>
      <h1>Enter at least 10 characters to move on...</h1>
      <input
        type='textbox' 
        value={stepTwoString}
        onChange={(e) => setStepTwoString(e.target.value)}
      />
    </>
    )
}

const Step3 =  ({stepThreeString, setStepThreeString}) => {
  return (
    <>
      <h1>Enter at least 15 characters and no letter E</h1>
      <input
        type='textbox' 
        value={stepThreeString}
        onChange={(e) => setStepThreeString(e.target.value)}
      />
    </>
    )
}
const Step4 = ({stepTwoString, stepThreeString}) => {
  
  return (
  <>
    <h1>You entered the following strings</h1>
    <code style={{display: 'block'}}>Step 2: {stepTwoString}</code>
    <code style={{display: 'block'}}>Step 3: {stepThreeString}</code>
  </>
  )
}

export default App;
