import React, {useState} from 'react'

const SteppedModal = ({navigation, steps, stepsComplete, validation, submitAction}) => {
    const [currentStep, setCurrentStep] = useState(0);
    
    function handleClick() {
        setCurrentStep(currentStep + 1);
    }

    function isLastStep(){
        return currentStep === navigation.length -1
    }

    return (<div style={{boxShadow: '0 0 20px rgba(0,0,0,0.2)', width: '600px', height: '600px', margin: '75px auto'}}>
        <ul style={{display: 'flex', listStyle:'none'}}>
            {navigation.map((navItem, idx) => {
            return (
            <li key={idx}>
                <button onClick={() => setCurrentStep(idx)} disabled={!stepsComplete[idx]} style={{opacity: !stepsComplete[idx] ? 0.5 : 1}}>{navItem}</button>
            </li>)
            })}
        </ul>
        {steps[currentStep]}
        <div style={{display:'block'}}>
            <button onClick={isLastStep() ? submitAction : handleClick} disabled={!validation[currentStep]} style={{opacity: !validation[currentStep] ? 0.5 : 1}}>{isLastStep() ? 'Submit' : 'Next'}</button>
        </div>
    </div>)
}

export default SteppedModal